package view;

import control.Handler;
import control.MenuItems;
import model.io.InputImage;

import javax.swing.*;
import java.awt.*;

/**
 * Created by majid on 5/16/17.
 */
public class Menu extends JFrame {
    private JMenuBar menuBar;
    private JMenu menu;
    private JMenuItem menuItem;

    public Menu() {
        super("Menu");

        Handler handler;

        setLayout(new FlowLayout());

        menuBar = new JMenuBar();

        menu = new JMenu("File");
        menuBar.add(menu);

        menuItem = new JMenuItem("New");
        menu.add(menuItem);
        handler = new Handler(MenuItems.New);
        menuItem.addActionListener(handler);

        menuItem = new JMenuItem("Open");
        menu.add(menuItem);
        handler = new Handler(MenuItems.Open);
        menuItem.addActionListener(handler);

        menuItem = new JMenuItem("Save");
        menu.add(menuItem);
        handler = new Handler(MenuItems.Save);
        menuItem.addActionListener(handler);

        menuItem = new JMenuItem("Close");
        menu.add(menuItem);
        handler = new Handler(MenuItems.Close);
        menuItem.addActionListener(handler);

        menu = new JMenu("Edit");
        menuBar.add(menu);

        menuItem = new JMenuItem("Rotate");
        menu.add(menuItem);

        menuItem = new JMenuItem("ImageCrop");
        menu.add(menuItem);

        menuItem = new JMenuItem("Filter");
        menu.add(menuItem);

        menuItem = new JMenuItem("Color");
        menu.add(menuItem);

        add(menuBar);




        //add(new InputImage().getLabel());

    }
}
