package model.io;

import javax.swing.*;

/**
 * Created by majid on 5/16/17.
 */
public class InputImage {
    private JLabel label;
    public InputImage() {
        String  path ;
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("choosertitle");
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        path = String.valueOf(chooser.getSelectedFile());

        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
            System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
        } else {
            System.out.println("No Selection ");
        }

        ImageIcon imageIcon = new ImageIcon(String.valueOf(chooser.getSelectedFile()));
        label = new JLabel(imageIcon);
    }

    public JLabel getLabel() {
        return label;
    }
}
