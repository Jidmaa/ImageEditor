package model.edit;

import com.sun.scenario.effect.Crop;

import javax.swing.*;
import java.awt.*;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

/**
 * Created by majid on 6/2/17.
 */
public class ImageCrop extends JComponent implements MouseListener,MouseMotionListener {
    private BufferedImage bufferedImage;
    private int x1,y1,x2,y2;
    boolean cropping;

    public ImageCrop(BufferedImage bufferedImage){
        this.bufferedImage = bufferedImage;
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    public void setImage(BufferedImage bufferedImage){
        this.bufferedImage = bufferedImage;
    }

    public BufferedImage getImage(){
        return bufferedImage;
    }

    public void paintComponent(Graphics graphics){
        graphics.drawImage(bufferedImage,0,0,this);

        //change if statement

        if (cropping){
            graphics.setColor(Color.RED);
            graphics.drawRect(Math.min(x1,x2),Math.min(y1,y2),Math.max(x1,x2),Math.max(y1,y2));
        }
    }
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.x1 = e.getX();
        this.y1 = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.cropping = false;


    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
