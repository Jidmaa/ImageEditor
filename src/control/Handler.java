package control;

import model.io.InputImage;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by majid on 5/22/17.
 */
public class Handler extends AbstractAction {
    Enum name;

    public Handler(Enum name) {
        this.name = name;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (name.equals(MenuItems.New)) {

        } else if (name.equals(MenuItems.Open)) {
            InputImage inputImage = new InputImage();
            inputImage.getLabel();
        } else if (name.equals(MenuItems.Save)) {

        } else if (name.equals(MenuItems.Close)) {
            System.exit(0);
        }

    }
}
