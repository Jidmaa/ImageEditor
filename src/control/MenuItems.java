package control;

/**
 * Created by majid on 5/22/17.
 */
public enum MenuItems {
    New,
    Open,
    Save,
    Close
}
